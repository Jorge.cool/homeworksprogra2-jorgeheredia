package LinkedListImp;

import java.util.Objects;

/**
 * @author JorgeHeredia
 */
class Node<T> {
    T data;
    Node next = null;

    Node(final T data) {
        this.data = data;
    }

    public Node(T data, Node next) {
        this.data = data;
        this.next = next;
    }

    /**
     * This method change to form of print the node.
     * @return string of the node.
     */
    public String toString() {
        return "{Data = " + data + "; next = " + next + "}";
    }
}
