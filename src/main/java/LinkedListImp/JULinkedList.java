package LinkedListImp;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JULinkedList<T> implements List<T> {

    private Node<T> root;
    private int size = 0;

    public String toString() {
        return "List(" + size + "): " + root;
    }

    //required Methods
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return !(size() > 0);
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> iteratorNode = root;
            @Override
            public boolean hasNext() {
                boolean hasNext = false;
                if (iteratorNode != null) {
                    hasNext = iteratorNode.next != null;
                }
                return hasNext;
            }

            @Override
            public T next() {
                if (hasNext()) {
                    iteratorNode = iteratorNode.next;
                } else {
                    iteratorNode = null;
                }
                return iteratorNode.data;
            }
        };
    }

    @Override
    public boolean add(T t) {
        Node<T> newNode = new Node<T>(t);
        if (isEmpty()) root = newNode;
        else  {
            for (Node<T> iteratorNode = root; iteratorNode != null; iteratorNode = iteratorNode.next) {
                if (iteratorNode.next == null) {
                    iteratorNode.next = newNode;
                    break;
                }
            }
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        boolean removed = false;
        for (Node<T> iteratorNode = root, previusNode = iteratorNode; iteratorNode != null;
             previusNode = iteratorNode, iteratorNode = iteratorNode.next){
            if (iteratorNode.data == o) {
                previusNode.next = iteratorNode.next;
                removed = true;
            }
        }
        if (removed) size--;
        return removed;
    }

    @Override
    public void clear() {
        root = null;
        size = 0;
    }

    @Override
    public T get(int i) {
        int count = 0;
        if (!(isEmpty() || i >= size || i < 0)) {
            for (Node<T> iteratorNode = root; iteratorNode != null; iteratorNode = iteratorNode.next) {
                if (count == i) return iteratorNode.data;
                count++;
            }
        }
        return null;
    }

    @Override
    public T remove(int i) {
        int count = 0;
        T elementRemoved = null;
        if (!(isEmpty() || i >= size || i < 0)) {
            for (Node<T> iteratorNode = root, previusNode = iteratorNode; iteratorNode != null;
                 previusNode = iteratorNode, iteratorNode = iteratorNode.next) {
                if (count == i) {
                    elementRemoved = iteratorNode.data;
                    previusNode.next = iteratorNode.next;
                    size--;
                    break;
                }
                count++;
            }
        }
        return elementRemoved;
    }

    @Override
    public boolean contains(Object o) {
        boolean contains = false;
        for (Node<T> iteratorNode = root; iteratorNode != null; iteratorNode = iteratorNode.next) {
            if (iteratorNode.data == o) {
                contains = true;
                break;
            }
        }
        return contains;
    }

    @Override
    public T set(int i, T t) {
        int count = 0;
        T elementChanged = null;
        if (!(isEmpty() || i >= size || i < 0)) {
            for (Node<T> iteratorNode = root; iteratorNode != null; iteratorNode = iteratorNode.next) {
                if (count == i) {
                    elementChanged = iteratorNode.data;
                    iteratorNode.data = t;
                    break;
                }
                count++;
            }
        }
        return elementChanged;
    }

    @Override
    public int indexOf(Object o) {
        int index = 0;
        boolean found = false;
        for (Node<T> iteratorNode = root; iteratorNode != null; iteratorNode = iteratorNode.next) {
            if (iteratorNode.data == o) {
                found = true;
                break;
            }
            index++;
        }
        if (!found) index = -1;
        return index;
    }

    @Override
    public int lastIndexOf(Object o) {
        int index = 0;
        int lastIndex = 0;
        boolean found = false;
        for (Node<T> iteratorNode = root; iteratorNode != null; iteratorNode = iteratorNode.next) {
            if (iteratorNode.data == o) {
                found = true;
                lastIndex = index;
            }
            index++;
        }
        if (!found) lastIndex = -1;
        return lastIndex;
    }


    //Optional methods
    @Override
    public void add(int i, T t) {
        int count = 0;
        if (!(isEmpty() || i < 0)) {
            if (i == size) {
                add(t);
            } else if (i > size) {
                System.out.println("Error: the index is larger that the list size");
            } else {
                Node<T> newNode = new Node<T>(t);
                for (Node<T> iteratorNode = root, previusNode = iteratorNode; iteratorNode != null;
                     previusNode = iteratorNode, iteratorNode = iteratorNode.next) {
                    if (count == i) {
                        newNode.next = iteratorNode;
                        previusNode.next = newNode;
                        size++;
                        break;
                    }
                    count++;
                }
            }
        }
    }

    @Override
    public Object[] toArray() {
        Object[] returnedArray = new Object[size()];
        for (int i = 0; i < size(); i++) {
            returnedArray[i] = get(i);
        }
        return returnedArray;
    }

    @Override
    public <T1> T1[] toArray(T1[] t1s) {
        return null;
    }

    @Override
    public List<T> subList(int i, int i1) {
        return null;
    }

    //Methods not required
    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> collection) {
        return false;
    }

    @Override
    public boolean addAll(int i, Collection<? extends T> collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int i) {
        return null;
    }
}
