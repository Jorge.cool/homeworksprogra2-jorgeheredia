package bagSorts;

public class Main {
    public static void main(String[] args) {
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(5);
        bag.add(3);
        bag.add(1);
        bag.add(2);
        bag.add(3);
        System.out.println(bag);
        bag.xChange(1, 3);
        System.out.println(bag);
        //(5)root={data=1, sig->{data=2, sig->{data=3, sig->{data=3, sig->{data=5, sig->null}}}}}
        bag.bubbleSort();
        System.out.println(bag);
    }
}
