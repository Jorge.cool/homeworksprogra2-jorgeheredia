package bagSorts;

public interface Bag<T extends Comparable<T>> {
    boolean add(T data);
    void selectionSort();
    void bubbleSort();
    void xChange(int i, int j);
}
class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data; }
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}
class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;
        return true;
    }
    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }
    
    public void selectionSort() {
        int positionMinor = 0;
        Node<T> iteratorNode = root;
        Node<T> nodeMinor = root;
        boolean changePossible = false;
        for (int i = 0; i < size - 1; i++, iteratorNode = iteratorNode.getNext()) {
            Node<T> iteratorNode2 = iteratorNode;
            for (int j = i; j <= size - 1; j++, iteratorNode2 = iteratorNode2.getNext()) {
                if (nodeMinor.getData().compareTo(iteratorNode2.getData()) > 0) {
                    nodeMinor = iteratorNode2;
                    positionMinor = j;
                    changePossible = true;
                }
            }
            if (changePossible) {
                xChange(i, positionMinor);
                iteratorNode = nodeMinor;
                changePossible = false;
            }
            nodeMinor = nodeMinor.getNext();
        }
    }

    public void bubbleSort() {
        //Node<T> minorNode = iteratorNode;
        for (int j = 0; j < size - 1; j++) {
            Node<T> iteratorNode = root;
            for (int i = 0; i < size - j - 1; i++, iteratorNode = iteratorNode.getNext()) {
                if (iteratorNode.getData().compareTo(iteratorNode.getNext().getData()) > 0) {
                    iteratorNode = iteratorNode.getNext();
                    xChange(i, i + 1);;
                }
            }
        }
    }

    @Override
    public void xChange(int i, int j) {
        int count = 0;
        if (size > 0 || i < size && j < size) {
            if (i == 0) {
                if (j == 1) {
                    Node<T> temp = root.getNext();
                    root.setNext(root.getNext().getNext());
                    temp.setNext(root);
                    root = temp;
                } else {
                    Node<T> iteratorNode = root;
                    Node<T> previusNode = iteratorNode;
                    for (int k = 0; k < j; k++, previusNode = iteratorNode, iteratorNode = iteratorNode.getNext()) {
                    }
                    Node<T> temp = root.getNext();
                    root.setNext(iteratorNode.getNext());
                    previusNode.setNext(root);
                    iteratorNode.setNext(temp);
                    root = iteratorNode;
                }
            } else {
                for (Node<T> iteratorNode = root, previusNode = iteratorNode; count <= i;
                     previusNode = iteratorNode, iteratorNode = iteratorNode.getNext(), count++) {
                    if (count == i) {
                        int count2 = 0;
                        for (Node<T> iteratorNode2 = root, previusNode2 = iteratorNode2; count2 <= j;
                             previusNode2 = iteratorNode2, iteratorNode2 = iteratorNode2.getNext(), count2++) {
                            if (count2 == j) {
                                // If either x or y is not present, nothing to do
                                if (iteratorNode == null || iteratorNode2 == null)
                                    return;

                                // Swap next pointers
                                previusNode.setNext(iteratorNode2);
                                previusNode2.setNext(iteratorNode);
                                Node<T> temp = iteratorNode.getNext();
                                iteratorNode.setNext(iteratorNode2.getNext());
                                iteratorNode2.setNext(temp);

                            }
                        }
                    }
                }
            }
        }
    }
}