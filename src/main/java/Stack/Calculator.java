package Stack;

import Stack.Exceptions.*;

public class Calculator {
    private int result = 0;
    private Stack<Character> stackOfNumber = new Stack<Character>();
    private Stack<Character> stackOfOperator = new Stack<Character>();
    private int numberParenthesis;

    private void getTokens(String str) throws CalculatorException {
        char previousValue = '0';
        for (int i = 0; i < str.length(); i++) {
            if (i != 0) previousValue = str.charAt(i - 1);
            validate(previousValue, str.charAt(i));
            if (isOperator(str.charAt(i))) {
                if (stackOfOperator.getHead() != null) {
                    if (priority(str.charAt(i)) < priority(stackOfOperator.getHead().getData())) {
                        previousValue = stackOfOperator.pop();
                        stackOfOperator.push(str.charAt(i));
                        stackOfOperator.push(previousValue);
                        enterNumber(priority(str.charAt(i)), str.charAt(i - 1), str.charAt(i + 1));
                    } else {
                        stackOfOperator.push(str.charAt(i));
                        char nextValue = 0;
                        if (!(str.charAt(i) == '!')) nextValue = str.charAt(i + 1);
                        enterNumber(priority(str.charAt(i)), str.charAt(i - 1), nextValue);
                    }
                } else {
                    if (!(str.charAt(i) == '!')) {
                        stackOfNumber.push(str.charAt(i - 1));
                    }
                    stackOfOperator.push(str.charAt(i));
                    if (str.charAt(i) == '!') enterNumber(priority(str.charAt(i)), str.charAt(i - 1), '0');
                    else enterNumber(priority(str.charAt(i)), str.charAt(i - 1), str.charAt(i + 1));
                }
            }
        }
        if (numberParenthesis != 0) {
            throw new ParenthesisSyntaxException("The parentheses were not placed correctly");
        }
    }

    public int calculate(String string) throws CalculatorException {
        getTokens(string);
        int index = 0;
        stackOfNumber = reverse(stackOfNumber);
        while (stackOfOperator.getHead() != null) {
            char operator = stackOfOperator.pop();
            if ((!(operator == '!') && index == 0)) result = stackOfNumber.pop() - '0';
            char number = stackOfNumber.pop();
            if (operator == '!' && index == 0) {
                result = 1;
            }
            switch (operator) {
                case '!':
                    for (int i = (number -'0'); i > 0; i--) {
                        result *= i;
                    }
                    break;
                case '^':
                    int valuePower = result;
                    for (int i = 1; i < number - '0'; i++) {
                        result *= valuePower;
                    }
                    break;
                case '+':
                    result += number - '0';
                    break;
                case '*':
                    result *= number - '0';
            }
            index++;
        }
        return result;
    }

    private Stack<Character> reverse(Stack<Character> stack) {
        Stack<Character> newStack = new Stack<Character>();
        int sizeStack = stack.getSize();
        for (int i = 0; i < sizeStack; i++) {
            newStack.push(stack.pop());
        }
        return newStack;
    }

    private void enterNumber(int priority, char previousValue, char nextValue) {
        if (priority == 4) {
            stackOfNumber.push(previousValue);
        } else {
            if (!isOperator(nextValue)) {
                stackOfNumber.push(nextValue);
            }
        }
    }

    private int priority(char operator) {
        switch (operator) {
            case '!': return 4;
            case '^': return 3;
            case '+': return 2;
            case '*': return 1;
            default:
                return 0;
        }
    }

    private boolean isOperator(char character) {
        return character == '!' || character == '^' || character == '*' || character == '+';
    }

    private boolean isNumber(char character) {
        int value = character - '0';
        return value < 10 && value >= 0;
    }

    public void reset() {
        result = 0;
        stackOfOperator = new Stack<Character>();
        stackOfNumber = new Stack<Character>();
    }

    //VALIDACIÓN

    private void validate(char previousCharacter, char character) throws CalculatorException {
        counterParenthesis(character);
        validateOperators(character);
        validateOperatorsNoRepeat(previousCharacter, character);
    }

    private void validateOperators(char character) throws CalculatorException {
        switch (character) {
            case '/':
            case '-':
                throw new OperatorNotPermitted("The operator: '" + character + "'\tIS NOT PERMITTED");
        }
    }

    private void validateOperatorsNoRepeat(char previousCharacter, char character) throws CalculatorException {
        if (!(previousCharacter >= 48 && previousCharacter <= 57)) {
            switch (previousCharacter) {
                case '!':
                case '(':
                case ')':
                    break;
                default:
                    if (!(character >= 48 && character <= 57 || character >= 33 && character <= 41)) {
                        throw new RepeatedOperator(
                                "The operators '" + previousCharacter + "' and '" + character + "' they can't be together");
                    }
            }
        }
    }

    private void counterParenthesis(char character) {
        if (character == '(') numberParenthesis++;
        else if (character == ')') numberParenthesis--;
    }
}