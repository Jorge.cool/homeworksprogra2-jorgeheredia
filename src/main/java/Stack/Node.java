package Stack;

public class Node<T> {
    private T data;
    private Node<T> nextNode = null;

    public Node(T data) {
        this.data = data;
    }

    public Node(T data, Node<T> nextNode) {
        this.data = data;
        this.nextNode = nextNode;
    }

    @Override
    public String toString() {
        return "[data = " + data + " next = " + nextNode + "]";
    }

    public Node<T> getNextNode() {
        return nextNode;
    }

    public T getData() {
        return data;
    }

    public void setNextNode(Node<T> nextNode) {
        this.nextNode = nextNode;
    }
}