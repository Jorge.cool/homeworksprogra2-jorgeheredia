package Stack.Exceptions;

public class OperatorNotPermitted extends CalculatorException{
    public OperatorNotPermitted(String msg) {
        super(msg);
    }
}
