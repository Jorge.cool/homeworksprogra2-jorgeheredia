package Stack.Exceptions;

public class RepeatedOperator extends CalculatorException{
    public RepeatedOperator(String msg) {
        super(msg);
    }
}
