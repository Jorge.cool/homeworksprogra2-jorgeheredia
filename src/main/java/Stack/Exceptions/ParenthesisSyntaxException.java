package Stack.Exceptions;

public class ParenthesisSyntaxException extends CalculatorException {
    public ParenthesisSyntaxException(String msg) {
        super(msg);
    }
}
