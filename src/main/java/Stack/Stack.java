package Stack;

public class Stack<T extends Comparable<T> >{
    private int size;
    private Node<T> head;

    public Stack() {
        this.size = 0;
        this.head = null;
    }

    public void push(T data) {
        Node<T> newNode = new Node<T>(data);
        if (head != null) {
            newNode.setNextNode(head);
        }
        head = newNode;
        size++;
    }

    public T pop() {
        T valuePop = head.getData();
        head = head.getNextNode();
        size--;
        return valuePop;
    }

    public int getSize() {
        return size;
    }

    public Node<T> getHead() {
        return head;
    }

    @Override
    public String toString() {
        String returned = "[";
        for (Node<T> iteratorNode = head; iteratorNode != null; iteratorNode = iteratorNode.getNextNode()) {
            if (iteratorNode.getNextNode() == null) returned += iteratorNode.getData().toString();
            else returned += iteratorNode.getData().toString() + ", ";
        }
        returned += "]";
        return returned;
    }
}
