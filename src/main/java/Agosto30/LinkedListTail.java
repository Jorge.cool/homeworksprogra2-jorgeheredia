package Agosto30;

public class LinkedListTail<T extends Comparable<T>> {

    Node<T> head;
    Node<T> tail;

    public LinkedListTail() {
        this.head = null;
        this.tail = null;
    }

    public boolean searchDuplicate(T data) {
        boolean isDuplicated = false;
        for (Node<T> iteratorNode = head; iteratorNode != null; iteratorNode = iteratorNode.getNext()) {
            if (data.compareTo(iteratorNode.getData()) == 0) {
                isDuplicated = true;
                break;
            }
        }
        return isDuplicated;
    }
    
    public void add(T data) throws DuplicatedException {
        if (searchDuplicate(data)) {
            throw new DuplicatedException("No se admiten valores duplicados");
            //System.out.println("not admitted");
        } else {
            Node<T> newNode = new Node<>(data);
            if (head == null)
                head = newNode;
            else
                tail.setNext(newNode);
            tail = newNode;
        }
    }

    public T deque() {
        T firstValue = head.getData();
        if (head != null) head = head.getNext();
        return firstValue;
    }
    
    public LinkedListTail<T> intercalate(LinkedListTail<T> tail1, LinkedListTail<T> tail2) throws DuplicatedException {
        LinkedListTail<T> tailFinal = new LinkedListTail<>();
        while (tail1.head != null) {
            tailFinal.add(tail1.deque());
            tailFinal.add(tail2.deque());
        }
        return tailFinal;
    }

    public static void main(String[] args) throws DuplicatedException {
        LinkedListTail<Integer> linkedListTail = new LinkedListTail<>();
        //System.out.println(linkedListTail.head);
        linkedListTail.add(1);
        linkedListTail.add(2);
        linkedListTail.add(3);
        System.out.println("TODO BIEN");
        System.out.println(linkedListTail.head + "\n");

        try {
            linkedListTail.add(1);
        } catch (DuplicatedException e) {
            System.out.println(e);
        }
    }
}
