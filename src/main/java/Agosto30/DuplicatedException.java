package Agosto30;

public class DuplicatedException extends Exception{
    public DuplicatedException (String msg) {
        super(msg);
    }
}
