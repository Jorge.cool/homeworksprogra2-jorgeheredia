package Agosto0822;

public class Main {
    public static void main(String[] args) {
        // 1.- sacar screenshot de la salida del metodo
        // 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
        LlamadasEncadenadas.doOne();

        // descomentar la llamda a: Infinito.whileTrue()
        // 1.- sacar screenshot de la exception
        // 2.- a continuacion describir el problema:
        // Descripcion: StackOverFlowError, normalmente se da este problema cuando se entra en un bucle infinito y la
        //              aplicación no sabe como responder, la pila de la app esta agotada o llena.
        // En este caso nos muestra la linea 5 de infinito, donde hay un while que nunca se rompe.
        // 3.- volver a comentar la llmanda a: Infinito.whileTrue()
        // Infinito.whileTrue();

        // descomentar la llamda a: Contador.contarHasta(), pasnado un humer entero positivo
        // 1.- sacar screenshot de la salida del metodo
        // 2.- a continuacion describir como funciona este metodo:
        // Descripcion: Este metodo funciona gracias a la recursividad de java, ya que le pasamos un numero, lo imprime
        //              y si el numero es mayor a 0 vuelve a llamar a la misma funcion pero restandole en uno al numero.
        Contador.contarHasta(5);

        System.out.println(RecursividadvsIteracion.factorialIter(5));
        System.out.println(RecursividadvsIteracion.factorialRecu(5));


    }
}

