package GameBoard;

public class Obstacle extends BoardObject {

    public Obstacle(int position_x, int position_y) {
        super.position_x = position_x;
        super.position_y = position_y;
        super.shape = 'x';
    }
}
