package GameBoard;

public class Player extends BoardObject {
    private String name, lastName;
    private int score;

    public Player(String name, String lastName) {
        this.name = name;
        this.lastName = lastName;
        this.score = 0;
        this.shape = 'P';
    }

    public void showInfoPlayer() {
        System.out.println("Name: " + name + "\nScore: " + score);
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public void scoreWin() {
        score += 100;
    }
}
