package GameBoard;

public class Board {
    private int width, height;
    private char[][] board;
    private char shapeObstacle;
    private Player player;

    public Board(int width, int height) {
        this.width = width;
        this.height = height;
        board = new char[width][height];
        addObstacles(5);
    }

    public void addPlayer(Player player) {
        this.player = player;
        board[0][0] = player.getShape();
        player.setPosition_x(0);
        player.setPosition_y(0);
    }

    public void changePositionPlayer(int position_y, int position_x) {
        if (board[position_y][position_x] != shapeObstacle) {
            board[player.getPosition_y()][player.getPosition_x()] = ' ';
            board[position_y][position_x] = player.getShape();

            player.setPosition_y(position_y);
            player.setPosition_x(position_x);
            System.out.println("move successful");
        } else {
            System.out.println("Occupied");
        }

    }

    public void addObstacles(int numberObstacles) {
        for (int i = 0; i < numberObstacles; i++) {
            int position_x = (int) ((Math.random() * (10 + 1)));
            int position_y = (int) ((Math.random() * (10 + 1)));
            if (position_x != 10 && position_y != 10) {
                Obstacle obstacle = new Obstacle(position_x, position_y);
                shapeObstacle = obstacle.getShape();
                board[position_y][position_x] = obstacle.getShape();
            }
        }
    }

    public void printBoard() {
        String boardPrint = "";
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                boardPrint += " " +  board[i][j] + " ";
            }
            boardPrint += "\n";
        }

        System.out.println(boardPrint);
    }
}
