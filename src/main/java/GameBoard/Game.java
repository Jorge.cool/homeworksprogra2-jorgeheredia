package GameBoard;

public class Game {
    Board board;
    Player player;

    public Game(Player player) {
        this.board = new Board(10, 10);
        this.player = player;
        board.addPlayer(player);
    }

    public void showBoard() {
        board.printBoard();
    }

    public void verifyWin() {
        if (player.getPosition_y() == 9 && player.getPosition_x() == 9) {
            System.out.println("You win!!!");
            player.scoreWin();
            player.showInfoPlayer();
        }
    }

    public void moveUp() {
        board.changePositionPlayer(player.getPosition_y() - 1, player.getPosition_x());
        showBoard();
        verifyWin();
    }

    public void moveDown() {
        board.changePositionPlayer(player.getPosition_y() + 1, player.getPosition_x());
        showBoard();
        verifyWin();
    }

    public void moveLeft() {
        board.changePositionPlayer(player.getPosition_y(), player.getPosition_x() - 1);
        showBoard();
        verifyWin();
    }

    public void moveRight() {
        board.changePositionPlayer(player.getPosition_y(), player.getPosition_x() + 1);
        showBoard();
        verifyWin();
    }

    public static void main(String[] args) {
        Player player = new Player("Jorge", "Heredia");
        Game game = new Game(player);

        game.moveDown();
        game.moveDown();
        game.moveRight();
        game.moveRight();
        game.moveDown();
        game.moveDown();
        game.moveDown();
        game.moveDown();
        game.moveDown();
        game.moveDown();
        game.moveDown();
        game.moveRight();
        game.moveRight();
        game.moveRight();
        game.moveRight();
        game.moveRight();
        game.moveRight();
        game.moveRight();
    }
}
