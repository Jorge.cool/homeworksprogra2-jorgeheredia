package GetNth;

public class Node{
    public int data;
    public Node next = null;

    public String toString() {
        return "{Data = " + data + "; next = " + next + "}";
    }

    public static int getNth(Node n, int index) throws Exception{
        int counter = 0;
        while (counter != index && n.next != null) {
            n = n.next;
            counter++;
        }

        if (index > counter){
            return Integer.parseInt(null);
        }
        return n.data;
    }
}