package ArraysPractice;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

class JUArrayList implements List<Integer> {
    private int[] data = new int[10];
    static int count = 0;

    public JUArrayList() {
        count = 0;
    }

    public void printArray() {
        String elements = "";
        for (int i = 0; i < count; i++) {
            elements += "Indice: " + i + "\t|\tElemento: " + data[i] + "\n";
        }

        System.out.println(elements);
    }


    @Override
    public boolean isEmpty() {
        boolean isEmpty = true;
        //Other solution
        if(count != 0) {
          isEmpty = false;
        }
        /*for (Integer integer : data) {
            if (integer != null) {
                isEmpty = false;
                break;
            }
        }*/
        return isEmpty;
    }

    @Override
    public Iterator<Integer> iterator() {
        Iterator<Integer> iterator = new Iterator<Integer>() {
            int counter = 0;
            @Override
            public boolean hasNext() {
                return counter < count;
            }

            @Override
            public Integer next() {
                counter ++;
                return data[counter];
            }
        };

        return iterator;
    }

    @Override
    public boolean add(Integer integer) {
        if (count == data.length) {
            int[] newData = new int[data.length + 10];
            for (int i = 0; i < data.length; i++) {
                newData[i] = data[i];
            }
            data = newData;
        }
        data[count] = integer;
        count++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        boolean canRemove = false;
        for (int i = 0; i < count; i++) {
            if (data[i] == (Integer) o) {
                canRemove = true;
            }
            if (canRemove && data.length != count) {
                data[i] = data[i + 1];
            }
        }

        if (canRemove){
            count--;
        }
        return canRemove;
    }

    @Override
    public void clear() {
        for (Integer integer : data) {
            integer = null;
            count = 0;
        }
    }

    @Override
    public Integer get(int i) {
        Integer integer = null;
        if (i < count) {
            integer = data[i];
        }
        return integer;
    }

    @Override
    public Integer remove(int i) {
        Integer integerDeleted = null;
        if (i < count) {
            integerDeleted = data[i];
            for (int j = i; j < count; j++) {
                data[j] = data[j + 1];
            }
            count--;
        }
        return integerDeleted;
    }

    //optionals

    @Override
    public boolean contains(Object o) {
        boolean contains = false;
        for (Integer integer : data) {
            if (integer.equals((Integer) o)){
                contains = true;
                break;
            }
        }
        return contains;
    }

    @Override
    public Integer set(int i, Integer integer) {
        Integer previousInteger = null;
        if (i < count) {
            previousInteger = data[i];
            data[i] = integer;
        } else if (i == count){
            add(integer);
        }
        return previousInteger;
    }

    @Override
    public void add(int i, Integer integer) {
        if (count == data.length) {
            int[] newData = new int[data.length + 10];
            for (int j = 0; j < data.length; j++) {
                newData[j] = data[j];
            }
            data = newData;
        }
        if (count != i) {
            Integer auxInteger = data[i];
            data[i] = integer;
            for (int j = count; j > i; j--) {
                data[j] = data[j - 1];
            }
            data[i + 1] = auxInteger;
        } else {
            add(integer);
        }

    }

    @Override
    public int size() {
        return count;
    }

    @Override
    public Object[] toArray() {
        Object[] objects = new Object[count];
        for (int i = 0; i < count; i++) {
            objects[i] = data[i];
        }
        return objects;
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return null;
    }

    //No necesarios
    @Override
    public boolean containsAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends Integer> collection) {
        return false;
    }

    @Override
    public boolean addAll(int i, Collection<? extends Integer> collection) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return false;
    }

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int i) {
        return null;
    }

    @Override
    public List<Integer> subList(int i, int i1) {
        return null;
    }
}
