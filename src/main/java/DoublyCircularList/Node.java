package DoublyCircularList;

public class Node<T> {
    private T data;
    private Node<T> previousNode = null;
    private Node<T> nextNode = null;

    public Node(T data) {
        this.data = data;
    }

    public Node(T data, Node<T> previousNode, Node<T> nextNode) {
        this.data = data;
        this.previousNode = previousNode;
        this.nextNode = nextNode;
    }

    @Override
    public String toString() {
        return "[" + data + "]";
    }

    public Node<T> getNextNode() {
        return nextNode;
    }

    public Node<T> getPreviousNode() {
        return previousNode;
    }

    public T getData() {
        return data;
    }

    public void setNextNode(Node<T> nextNode) {
        this.nextNode = nextNode;
    }

    public void setPreviousNode(Node<T> previousNode) {
        this.previousNode = previousNode;
    }
}
