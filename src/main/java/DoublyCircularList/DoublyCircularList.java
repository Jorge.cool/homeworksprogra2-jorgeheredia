package DoublyCircularList;

public class DoublyCircularList<T extends Comparable<T>> implements List<T>{
    private int size = 0;
    private Node<T> head = null;
    //Requerimiento letra: H
    //*Metodos pedidos:
    public DoublyCircularList(Node<T> head) {
        this.head = head;
        this.size = size();
    }

    @Override
    public String toString() {
        String listToString = "";
        Node<T> iteratorNode = head;
        for (int i = 0; i < size; i++, iteratorNode = iteratorNode.getNextNode()) {
            listToString += iteratorNode.toString();
        }
        return listToString;
    }

    @Override
    public int size() {
        if (head != null) {
            size = 1;
            if (head.getNextNode() != null) {
                for (Node<T> iteratorNode = head.getNextNode(); iteratorNode != head; iteratorNode = iteratorNode.getNextNode()) {
                    size++;
                }
            }
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public T get(int index) {
        //turned a long index into a short index, because it's more expensive to iterate a whole index
        //and loop the circular List. compared to a short index and a single lap.
        if (index > size) {
            index = index - ((index/size) * size);
        } else if (index < 0) {
            //Here we shorten the index and transform the negative index into a positive.
            index = size + (index + ((index/size) * size * -1));
        }

        Node<T> iteratorNode = head;
        for (int count = 0; count != index; iteratorNode = iteratorNode.getNextNode(), count++) {}
        return iteratorNode.getData();
    }

    @Override
    public void reverse() {
        Node<T> itemNewList = new Node<>(head.getData());
        Node<T> lastNewNode = itemNewList;
        for (Node<T> iteratorNode = head.getNextNode(); iteratorNode != head; iteratorNode = iteratorNode.getNextNode()) {
            Node<T> newNode = new Node<>(iteratorNode.getData());
            newNode.setNextNode(itemNewList);
            itemNewList.setPreviousNode(newNode);
            itemNewList = newNode;
        }
        lastNewNode.setNextNode(itemNewList);
        head = itemNewList;
    }

    @Override
    public void mergeSort() {
        //no pude
    }
    //No pedidos

    @Override
    public boolean add(T data) {
        return false;
    }

    @Override
    public boolean remove(T data) {
        return false;
    }

    @Override
    public void selectionSort() {

    }

    @Override
    public void bubbleSort() {

    }
}
