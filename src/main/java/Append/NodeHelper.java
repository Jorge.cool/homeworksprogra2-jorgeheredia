package Append;

public class NodeHelper {

    /**
     * This method build nodes from the array of integers.
     * All nodes concatenate.
     * @param list array
     * @return Node concatenated.
     */
    public static Node build(int[] list) {
        Node builtNode = new Node(list[0]);
        for (int i = 1; i <= list.length - 1; i++) {
            builtNode.getLast().next = new Node(list[i]);
        }

        return builtNode;
    }
}
