package Append;

import java.util.Objects;

/**
 * @author JorgeHeredia
 */
class Node {
    int data;
    Node next = null;

    Node(final int data) {
        this.data = data;
    }

    public Node(int data, Node next) {
        this.data = data;
        this.next = next;
    }

    /**
     * This method iterate node to find the last node.
     * @return last node
     */
    public Node getLast() {
        int counter = 0;
        Node nodeReturned = this;
        while (nodeReturned.next != null) {
            nodeReturned = nodeReturned.next;
        }

        return nodeReturned;
    }

    /**
     * This method change to form of print the node.
     * @return string of the node.
     */
    public String toString() {
        return "{Data = " + data + "; next = " + next + "}";
    }

    /**
     * This method iterate node counting how many nodes iterate(size).
     * @return size
     */
    public int size() {
        int count = 1;
        Node iteratorNode = next;
        while (iteratorNode != null) {
            iteratorNode = iteratorNode.next;
            count ++;
        }
        return count;
    }

    /**
     * This method tries to link two nodes if and only if one of the two nodes is not null.
     * @param listA any node(If this is null maybe not link)
     * @param listB any node(If this is null maybe not link)
     * @return Node linked with the two nodes.
     */
    public static Node append(Node listA, Node listB) {
        Node nodeReturned;
        if (listA != null && listB != null) {
            nodeReturned = listA;
            listA.getLast().next = listB;
        } else if (listA == null && listB != null) {
            nodeReturned = listB;
        } else if (listA != null && listB == null) {
            nodeReturned = listA;
        } else {
            nodeReturned = null;
        }
        return nodeReturned;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Node)) return false;
        Node node = (Node) o;
        return data == node.data && Objects.equals(next, node.next);
    }

    @Override
    public int hashCode() {
        return Objects.hash(data, next);
    }
}
