package LinkedListImp;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

//2 errores esperados impresos con sout
class JULinkedListTest {
    private List<String> listaString = new JULinkedList<String>();
    private List<Integer> listaInt = new JULinkedList<Integer>();

    void reset() {
        listaString.clear();
        listaInt.clear();
    }

    void usualList() {
        listaString.add("A");
        listaString.add("B");
        listaString.add("C");
        listaInt.add(1);
        listaInt.add(2);
        listaInt.add(3);
    }

    @Test
    void size() {
        reset();
        assertEquals(0, listaString.size());
        assertEquals(0, listaInt.size());

        usualList();

        assertEquals(3, listaString.size());
        assertEquals(3, listaInt.size());
        listaString.remove("A");
        assertEquals(2, listaString.size());
        listaInt.remove(0);
        assertEquals(2, listaInt.size());

    }

    @Test
    void isEmpty() {
        reset();
        assertTrue(listaString.isEmpty());
        assertTrue(listaInt.isEmpty());
        listaString.add("A");
        listaInt.add(1);
        assertFalse(listaString.isEmpty());
        assertFalse(listaInt.isEmpty());
    }

    @Test
    void iterator() {
        reset();
        assertFalse(listaString.iterator().hasNext());
        assertFalse(listaInt.iterator().hasNext());
        listaString.add("A");
        listaString.add("B");
        listaInt.add(1);
        listaInt.add(2);
        Iterator<Integer> iteratorInt = listaInt.iterator();
        Iterator<String> iteratorString = listaString.iterator();
        assertTrue(iteratorString.hasNext());
        assertTrue(iteratorInt.hasNext());
        assertEquals("B", iteratorString.next());
        assertEquals(2, iteratorInt.next());
    }

    @Test
    void add() {
        reset();
        listaString.add("A");
        listaString.add("B");
        assertEquals("List(2): {Data = A; next = {Data = B; next = null}}", listaString.toString());

        listaInt.add(1);
        listaInt.add(2);
        assertEquals("List(2): {Data = 1; next = {Data = 2; next = null}}", listaInt.toString());

    }

    //remove with object
    @Test
    void remove() {
        reset();
        usualList();

        assertEquals(3, listaString.size());
        assertEquals(3, listaInt.size());
        assertTrue(listaString.remove("B"));
        assertTrue(listaInt.remove(Integer.valueOf(2)));
        assertEquals(2, listaString.size());
        assertEquals(2, listaInt.size());

    }

    @Test
    void clear() {
        reset();
        usualList();

        assertEquals("List(3): {Data = A; next = {Data = B; next = {Data = C; next = null}}}", listaString.toString());
        assertEquals("List(3): {Data = 1; next = {Data = 2; next = {Data = 3; next = null}}}", listaInt.toString());
        listaString.clear();
        listaInt.clear();
        assertTrue(listaString.isEmpty());
        assertTrue(listaInt.isEmpty());
        assertEquals(0, listaString.size());
        assertEquals(0, listaInt.size());
    }

    @Test
    void get() {
        reset();
        usualList();

        assertEquals("A", listaString.get(0));
        assertEquals("C", listaString.get(2));
        assertEquals(null, listaString.get(5));
        assertEquals(null, listaString.get(-1));
    }

    //remove with index
    @Test
    void testRemove() {
        reset();
        usualList();

        assertEquals("B", listaString.remove(1));
        assertEquals(2, listaString.size());
        assertEquals("List(2): {Data = A; next = {Data = C; next = null}}", listaString.toString());
        assertNull(listaString.remove(5));
        assertNull(listaString.remove(-2));

        assertEquals(2, listaInt.remove(1));
        assertEquals(2, listaString.size());
        assertEquals("List(2): {Data = 1; next = {Data = 3; next = null}}", listaInt.toString());
        assertNull(listaInt.remove(8));
        assertNull(listaInt.remove(-3));

    }

    @Test
    void contains() {
        reset();
        usualList();

        assertTrue(listaString.contains("B"));
        assertFalse(listaString.contains("PAPA"));
        assertFalse(listaString.contains(2));

        assertTrue(listaInt.contains(2));
        assertFalse(listaInt.contains(28));
        assertFalse(listaInt.contains("Hello"));
        assertFalse(listaInt.contains("1"));
    }

    @Test
    void set() {
        reset();
        usualList();

        assertEquals("A", listaString.set(0, "PARTY"));
        assertEquals("List(3): {Data = PARTY; next = {Data = B; next = {Data = C; next = null}}}", listaString.toString());
        //In this method, I'm not sure what should happen in case the index entry is equal to the last node
        //Because the index input is empty and will occupy by a new Node, I thought this action is more adding than setting.
        assertEquals(null, listaString.set(3, "Bla"));

        assertEquals(2, listaInt.set(1, 7));
        assertEquals("List(3): {Data = 1; next = {Data = 7; next = {Data = 3; next = null}}}", listaInt.toString());
        assertEquals(null, listaInt.set(-4, 85));
    }

    @Test
    void indexOf() {
        reset();
        usualList();

        assertEquals(0, listaString.indexOf("A"));
        assertEquals(2, listaString.indexOf("C"));
        assertEquals(-1, listaString.indexOf("Beer"));

        assertEquals(1, listaInt.indexOf(2));
        assertEquals(2, listaInt.indexOf(3));
        assertEquals(-1, listaInt.indexOf(777));
    }

    @Test
    void lastIndexOf() {
        reset();
        usualList();
        listaString.add("Wine");
        listaString.add("A");
        listaString.add("Wine");

        assertEquals(4, listaString.lastIndexOf("A"));
        assertEquals(5, listaString.lastIndexOf("Wine"));
        assertEquals(-1, listaString.lastIndexOf("Beer"));

        listaInt.add(7);
        listaInt.add(2);
        listaInt.add(7);

        assertEquals(4, listaInt.lastIndexOf(2));
        assertEquals(5, listaInt.lastIndexOf(7));
        assertEquals(-1, listaInt.lastIndexOf(777));
    }

    @Test
    void testAdd() {
        reset();
        usualList();

        assertEquals("List(3): {Data = A; next = {Data = B; next = {Data = C; next = null}}}",
                listaString.toString());
        listaString.add(1, "Hello");
        assertEquals("List(4): {Data = A; next = {Data = Hello; next = {Data = B; next = " +
                "{Data = C; next = null}}}}", listaString.toString());
        listaString.add(4, "World");
        assertEquals("List(5): {Data = A; next = {Data = Hello; next = {Data = B; next = " +
                "{Data = C; next = {Data = World; next = null}}}}}", listaString.toString());
        listaString.add(7, "Code");
        assertEquals("List(5): {Data = A; next = {Data = Hello; next = {Data = B; next = " +
                "{Data = C; next = {Data = World; next = null}}}}}", listaString.toString());

        assertEquals("List(3): {Data = 1; next = {Data = 2; next = {Data = 3; next = null}}}",
                listaInt.toString());
        listaInt.add(1, 77);
        assertEquals("List(4): {Data = 1; next = {Data = 77; next = {Data = 2; next = " +
                "{Data = 3; next = null}}}}", listaInt.toString());
        listaInt.add(4, 23);
        assertEquals("List(5): {Data = 1; next = {Data = 77; next = {Data = 2; next = " +
                "{Data = 3; next = {Data = 23; next = null}}}}}", listaInt.toString());
        listaInt.add(10, 69);
        assertEquals("List(5): {Data = 1; next = {Data = 77; next = {Data = 2; next = " +
                "{Data = 3; next = {Data = 23; next = null}}}}}", listaInt.toString());
    }

    @Test
    void toArray() {
        reset();
        usualList();

        assertEquals(Arrays.toString(new String[]{"A", "B", "C"}), Arrays.toString(listaString.toArray()));
        assertEquals(Arrays.toString(new int[]{1, 2, 3}), Arrays.toString(listaInt.toArray()));
    }
}