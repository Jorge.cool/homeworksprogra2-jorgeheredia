import GameBoard.Game;
import GameBoard.Player;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestGame {
    @Test
    public void namePlayer() {
        Player player = new Player("Pepe", "Le'Peu");
        assertEquals("Pepe", player.getName());
    }

    @Test
    public void positionPlayer() {
        Player player = new Player("Pepe", "Le'Peu");
        player.setPosition_x(3);
        assertEquals(3, player.getPosition_x());
    }

    @Test
    public void game() {
        Player player = new Player("Jorge", "Heredia");
        Game game = new Game(player);

        game.moveDown();
        game.moveDown();
        game.moveRight();
        game.moveRight();
        game.moveDown();
        game.moveDown();
        game.moveDown();
        game.moveDown();
        game.moveDown();
        game.moveDown();
        game.moveDown();
        game.moveRight();
        game.moveRight();
        game.moveRight();
        game.moveRight();
        game.moveRight();
        game.moveRight();
        game.moveRight();
    }
}
