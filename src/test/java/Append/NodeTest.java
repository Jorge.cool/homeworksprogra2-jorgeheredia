package Append;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class NodeTest {
    //No encontre una dependencia NodeHelper y no se que hace exactamente.
    // Pero me cree una clase NodeHelper que nos ayuda con el metodo build de los tests.

    @Test
    public void twoEmpty() throws Exception {
        assertNull( Node.append( null, null ) );
    }

    @Test
    public void oneEmpty() throws Exception {
        System.out.println(Node.append( null, new Node( 1 ) ));
        assertEquals( Node.append( null, new Node( 1 ) ), new Node( 1 ) );
        assertEquals( Node.append( new Node( 1 ), null ), new Node( 1 ) );
    }

    @Test
    public void oneOne() throws Exception {
        assertEquals( Node.append( new Node( 1 ), new Node( 2 ) ), NodeHelper.build( new int[] { 1, 2 } ) );
        assertEquals( Node.append( new Node( 2 ), new Node( 1 ) ), NodeHelper.build( new int[] { 2, 1 } ) );
    }

    @Test
    public void bigLists() throws Exception {
        assertEquals(
                Node.append( NodeHelper.build( new int[] { 1, 2 } ), NodeHelper.build( new int[] { 3, 4 } ) ),
                NodeHelper.build( new int[] { 1, 2, 3, 4 } )
        );
        assertEquals(
                Node.append( NodeHelper.build( new int[] { 1, 2, 3, 4, 5 } ), NodeHelper.build( new int[] { 6, 7, 8 } ) ),
                NodeHelper.build( new int[] { 1, 2, 3, 4, 5, 6, 7, 8 } )
        );
    }

}
