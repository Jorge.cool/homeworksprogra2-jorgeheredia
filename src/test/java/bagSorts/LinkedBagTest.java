package bagSorts;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LinkedBagTest {
    @Test
    void selectionSort() {
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(5);
        bag.add(3);
        bag.add(1);
        bag.add(2);
        bag.add(3);
        bag.xChange(1, 3);
        assertEquals("(5)root={data=3, sig->{data=3, sig->{data=1, sig->{data=2, sig->{data=5, sig->null}}}}}", bag.toString());
        bag.selectionSort();
        assertEquals("(5)root={data=1, sig->{data=2, sig->{data=3, sig->{data=3, sig->{data=5, sig->null}}}}}", bag.toString());

        Bag<Integer> bag2 = new LinkedBag<>();
        bag2.add(1);
        bag2.add(2);
        bag2.add(3);
        assertEquals("(3)root={data=3, sig->{data=2, sig->{data=1, sig->null}}}", bag2.toString());
        bag2.selectionSort();
        assertEquals("(3)root={data=1, sig->{data=2, sig->{data=3, sig->null}}}", bag2.toString());
    }

    @Test
    void bubbleSort() {
        Bag<Integer> bag = new LinkedBag<>();
        bag.add(5);
        bag.add(3);
        bag.add(1);
        bag.add(2);
        bag.add(3);
        bag.xChange(1, 3);
        assertEquals("(5)root={data=3, sig->{data=3, sig->{data=1, sig->{data=2, sig->{data=5, sig->null}}}}}", bag.toString());
        bag.bubbleSort();
        assertEquals("(5)root={data=1, sig->{data=2, sig->{data=3, sig->{data=3, sig->{data=5, sig->null}}}}}", bag.toString());

        Bag<Integer> bag2 = new LinkedBag<>();
        bag2.add(1);
        bag2.add(2);
        bag2.add(3);
        assertEquals("(3)root={data=3, sig->{data=2, sig->{data=1, sig->null}}}", bag2.toString());
        bag2.bubbleSort();
        assertEquals("(3)root={data=1, sig->{data=2, sig->{data=3, sig->null}}}", bag2.toString());
    }
}