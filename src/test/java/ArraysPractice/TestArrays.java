package ArraysPractice;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestArrays {
    @Test
    public void testIsEmpty() {
        JUArrayList arrayList = new JUArrayList();
        assertTrue(arrayList.isEmpty());
        arrayList.add(12);
        assertFalse(arrayList.isEmpty());
        arrayList.remove(0);
        assertTrue(arrayList.isEmpty());
    }

    @Test
    public void testIterator() {
        JUArrayList arrayList = new JUArrayList();
        Iterator<Integer> iterator = arrayList.iterator();
        assertFalse(iterator.hasNext());
        arrayList.add(12);
        arrayList.add(13);
        arrayList.add(14);
        assertTrue(iterator.hasNext());
        assertEquals(13, iterator.next());
        assertEquals(14, iterator.next());
    }

    @Test
    public void testAdd() {
        JUArrayList arrayList = new JUArrayList();
        assertTrue(arrayList.add(7));
        arrayList.printArray();
        assertEquals(7, arrayList.get(0));
        assertTrue(arrayList.add(8));
        assertEquals(8, arrayList.get(1));
    }

    @Test
    public void testRemoveOB() {
        JUArrayList arrayList = new JUArrayList();
        arrayList.add(13);
        arrayList.add(17);
        arrayList.add(14);
        assertEquals(13, arrayList.get(0));
        Object object = 13;
        assertTrue(arrayList.remove(object));
        assertEquals(17, arrayList.get(0));
    }

    @Test
    public void testClear() {
        JUArrayList arrayList = new JUArrayList();
        arrayList.add(1);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(6);
        arrayList.add(7);

        assertFalse(arrayList.isEmpty());
        arrayList.clear();
        assertTrue(arrayList.isEmpty());
    }

    @Test
    public void testGet() {
        JUArrayList arrayList = new JUArrayList();
        arrayList.add(1);
        arrayList.add(3);
        arrayList.add(4);
        arrayList.add(5);
        arrayList.add(6);
        arrayList.add(7);

        assertEquals(1, arrayList.get(0));
        assertNull(arrayList.get(9)); //because never set value in this index
    }

    @Test
    public void testRemoveInt() {
        JUArrayList arrayList = new JUArrayList();
        arrayList.add(1);
        arrayList.add(3);
        arrayList.add(2);
        arrayList.add(3);

        assertEquals(3, arrayList.remove(1));
        assertEquals(2, arrayList.get(1));
    }

    @Test
    public void testContains() {
        JUArrayList arrayList = new JUArrayList();
        arrayList.add(1);
        arrayList.add(3);
        arrayList.add(2);
        arrayList.add(3);

        assertTrue(arrayList.contains(3));
        assertFalse(arrayList.contains(-7));
    }

    @Test
    public void testSet() {
        JUArrayList arrayList = new JUArrayList();
        arrayList.add(1);
        arrayList.add(3);
        arrayList.add(2);
        arrayList.add(3);

        assertEquals(3, arrayList.set(1, 7));
        assertEquals(7, arrayList.get(1));

        assertNull(arrayList.set(4, 7));
        assertEquals(7, arrayList.get(4));
    }

    @Test
    public void testAddInPosition() {
        JUArrayList arrayList = new JUArrayList();
        arrayList.add(1);
        arrayList.add(3);
        arrayList.add(2);
        arrayList.add(3);

        assertEquals(3, arrayList.get(1));
        assertEquals(2, arrayList.get(2));
        arrayList.add(1, 7);
        assertEquals(7, arrayList.get(1));
        assertEquals(3, arrayList.get(2));
    }

    @Test
    public void testSize() {
        JUArrayList arrayList = new JUArrayList();
        arrayList.add(1);
        arrayList.add(3);
        arrayList.add(2);
        arrayList.add(3);

        assertEquals(4, arrayList.size());
        arrayList.clear();
        assertEquals(0, arrayList.size());
    }
}
