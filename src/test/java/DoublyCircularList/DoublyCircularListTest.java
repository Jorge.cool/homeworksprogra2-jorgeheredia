package DoublyCircularList;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class DoublyCircularListTest {
    @Test
    public void sizeTest() {
        Node<Integer> node1 = new Node<>(3);
        Node<Integer> node2 = new Node<>(1);
        Node<Integer> node4 = new Node<>(2);
        Node<Integer> node3 = new Node<>(7);
        Node<Integer> node5 = new Node<>(8);

        node1.setPreviousNode(node5);
        node1.setNextNode(node2);
        node2.setPreviousNode(node1);
        node2.setNextNode(node3);
        node3.setPreviousNode(node2);
        node3.setNextNode(node4);
        node4.setPreviousNode(node3);
        node4.setNextNode(node5);
        node5.setPreviousNode(node4);
        node5.setNextNode(node1);

        List<Integer> list = new DoublyCircularList(node1);

        assertEquals(5, list.size());

        node4.setNextNode(node1);
        node1.setPreviousNode(node4);
        assertEquals(4, list.size());

        Node<String> node1S = new Node<>("A");
        Node<String> node2S = new Node<>("B");
        Node<String> node3S = new Node<>("C");
        Node<String> node4S = new Node<>("D");
        Node<String> node5S = new Node<>("E");

        node1S.setPreviousNode(node5S);
        node1S.setNextNode(node2S);
        node2S.setPreviousNode(node1S);
        node2S.setNextNode(node3S);
        node3S.setPreviousNode(node2S);
        node3S.setNextNode(node4S);
        node4S.setPreviousNode(node3S);
        node4S.setNextNode(node5S);
        node5S.setPreviousNode(node4S);
        node5S.setNextNode(node1S);

        List<String> listString = new DoublyCircularList(node1S);

        assertEquals(5, listString.size());

        node4S.setNextNode(node1S);
        node1S.setPreviousNode(node4S);
        assertEquals(4, listString.size());
    }

    @Test
    public void isEmptyTest() {
        List<Integer> list = new DoublyCircularList(null);
        assertTrue(list.isEmpty());

        Node<Integer> node1 = new Node<>(3);
        Node<Integer> node2 = new Node<>(1);
        Node<Integer> node3 = new Node<>(7);
        Node<Integer> node4 = new Node<>(2);
        Node<Integer> node5 = new Node<>(8);

        node1.setPreviousNode(node5);
        node1.setNextNode(node2);
        node2.setPreviousNode(node1);
        node2.setNextNode(node3);
        node3.setPreviousNode(node2);
        node3.setNextNode(node4);
        node4.setPreviousNode(node3);
        node4.setNextNode(node5);
        node5.setPreviousNode(node4);
        node5.setNextNode(node1);

        List<Integer> list2 = new DoublyCircularList(node1);
        assertFalse(list2.isEmpty());

        List<String> listString = new DoublyCircularList(null);
        assertTrue(listString.isEmpty());

        Node<String> node1S = new Node<>("A");
        Node<String> node2S = new Node<>("B");
        Node<String> node3S = new Node<>("C");
        Node<String> node4S = new Node<>("D");
        Node<String> node5S = new Node<>("E");

        node1S.setPreviousNode(node5S);
        node1S.setNextNode(node2S);
        node2S.setPreviousNode(node1S);
        node2S.setNextNode(node3S);
        node3S.setPreviousNode(node2S);
        node3S.setNextNode(node4S);
        node4S.setPreviousNode(node3S);
        node4S.setNextNode(node5S);
        node5S.setPreviousNode(node4S);
        node5S.setNextNode(node1S);

        List<Integer> listString2 = new DoublyCircularList(node1S);
        assertFalse(listString2.isEmpty());
    }

    @Test
    public void getTest() {
        Node<Integer> node1 = new Node<>(1);
        Node<Integer> node2 = new Node<>(2);
        Node<Integer> node4 = new Node<>(3);
        Node<Integer> node3 = new Node<>(4);
        Node<Integer> node5 = new Node<>(5);

        node1.setPreviousNode(node5);
        node1.setNextNode(node2);
        node2.setPreviousNode(node1);
        node2.setNextNode(node3);
        node3.setPreviousNode(node2);
        node3.setNextNode(node4);
        node4.setPreviousNode(node3);
        node4.setNextNode(node5);
        node5.setPreviousNode(node4);
        node5.setNextNode(node1);

        List<Integer> list = new DoublyCircularList(node1);
        assertEquals(2, list.get(1));
        assertEquals(5, list.get(-1));
        assertEquals(1, list.get(10));

        //Strings
        Node<String> node1S = new Node<>("A");
        Node<String> node2S = new Node<>("B");
        Node<String> node3S = new Node<>("C");
        Node<String> node4S = new Node<>("D");
        Node<String> node5S = new Node<>("E");

        node1S.setPreviousNode(node5S);
        node1S.setNextNode(node2S);
        node2S.setPreviousNode(node1S);
        node2S.setNextNode(node3S);
        node3S.setPreviousNode(node2S);
        node3S.setNextNode(node4S);
        node4S.setPreviousNode(node3S);
        node4S.setNextNode(node5S);
        node5S.setPreviousNode(node4S);
        node5S.setNextNode(node1S);

        List<String> listString = new DoublyCircularList(node1S);

        assertEquals("B", listString.get(1));
        assertEquals("E", listString.get(-1));
        assertEquals("A", listString.get(10));
    }

    @Test
    public void reverseTest() {
        Node<Integer> node1 = new Node<>(1);
        Node<Integer> node2 = new Node<>(2);
        Node<Integer> node3 = new Node<>(3);
        Node<Integer> node4 = new Node<>(4);
        Node<Integer> node5 = new Node<>(5);

        node1.setPreviousNode(node5);
        node1.setNextNode(node2);
        node2.setPreviousNode(node1);
        node2.setNextNode(node3);
        node3.setPreviousNode(node2);
        node3.setNextNode(node4);
        node4.setPreviousNode(node3);
        node4.setNextNode(node5);
        node5.setPreviousNode(node4);
        node5.setNextNode(node1);

        List<Integer> list = new DoublyCircularList(node1);
        assertEquals("[1][2][3][4][5]", list.toString());
        list.reverse();
        assertEquals("[5][4][3][2][1]", list.toString());

        //String
        Node<String> node1S = new Node<>("A");
        Node<String> node2S = new Node<>("B");
        Node<String> node3S = new Node<>("C");
        Node<String> node4S = new Node<>("D");
        Node<String> node5S = new Node<>("E");

        node1S.setPreviousNode(node5S);
        node1S.setNextNode(node2S);
        node2S.setPreviousNode(node1S);
        node2S.setNextNode(node3S);
        node3S.setPreviousNode(node2S);
        node3S.setNextNode(node4S);
        node4S.setPreviousNode(node3S);
        node4S.setNextNode(node5S);
        node5S.setPreviousNode(node4S);
        node5S.setNextNode(node1S);

        List<String> listString = new DoublyCircularList(node1S);

        assertEquals("[A][B][C][D][E]", listString.toString());
        listString.reverse();
        assertEquals("[E][D][C][B][A]", listString.toString());
    }
}

