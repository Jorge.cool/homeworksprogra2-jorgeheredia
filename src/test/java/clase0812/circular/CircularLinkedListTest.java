package clase0812.circular;

import clase0812.List;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CircularLinkedListTest {
    //ME TOCO HACER EL METODO REMOVE y toString echo
    @Test
    void remove() {
        List<Integer> list = CircularLinkedList.dummyList(5);
        //5,4,3,2,1
        assertFalse(list.isEmpty());
        assertEquals(5, list.size());
        assertEquals(5, list.get(0));
        assertEquals(5, list.get(5));
        assertTrue(list.remove(5));
        //4,3,2,1
        assertEquals(4, list.size());
        assertEquals(4, list.get(0));
        assertEquals(4, list.get(4));
        //4,3,2
        assertTrue(list.remove(1));
        assertEquals(3, list.size());
        assertEquals(4, list.get(0));
        assertEquals(3, list.get(4));
        assertTrue(list.remove(3));
        //4,2
        assertEquals(2, list.size());
        assertEquals(4, list.get(0));
        assertEquals(2, list.get(3));
        assertFalse(list.remove(3));
        //4,2
        assertEquals(2, list.size());
        assertEquals(4, list.get(0));
        assertEquals(2, list.get(3));
        assertTrue(list.remove(2));
        //4
        assertEquals(4, list.get(0));
        assertEquals(4, list.get(3));
    }

    @Test
    void toStringTest() {
        List<Integer> list = CircularLinkedList.dummyList(5);
        assertEquals("CircularList(5): 5; 4; 3; 2; 1; ", list.toString());

        List<Integer> list2 = CircularLinkedList.dummyList(8);
        assertEquals("CircularList(8): 8; 7; 6; 5; 4; 3; 2; 1; ", list2.toString());

    }
}
