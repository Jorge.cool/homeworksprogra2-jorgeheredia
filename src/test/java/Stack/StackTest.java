package Stack;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class StackTest {
    @Test
    public void stackCreate() {
        Stack<Integer> integerStack = new Stack<Integer>();
        assertNull(integerStack.getHead());
        assertEquals(0, integerStack.getSize());

        Stack<String> stringStack = new Stack<String>();
        assertNull(stringStack.getHead());
        assertEquals(0, stringStack.getSize());
    }

    @Test void pushTest() {
        Stack<Integer> integerStack = new Stack<Integer>();
        assertEquals(0, integerStack.getSize());

        integerStack.push(1);
        integerStack.push(2);
        integerStack.push(3);
        assertEquals(3, integerStack.getSize());
        assertEquals("[data = 3 next = [data = 2 next = [data = 1 next = null]]]",
                integerStack.getHead().toString());

        Stack<String> stringStack = new Stack<String>();
        assertEquals(0, stringStack.getSize());

        stringStack.push("A");
        stringStack.push("B");
        stringStack.push("C");
        assertEquals(3, stringStack.getSize());
        assertEquals("[data = C next = [data = B next = [data = A next = null]]]",
                stringStack.getHead().toString());
    }

    @Test
    public void popTest() {
        Stack<Integer> integerStack = new Stack<Integer>();
        integerStack.push(1);
        integerStack.push(2);
        integerStack.push(3);
        assertEquals("[data = 3 next = [data = 2 next = [data = 1 next = null]]]",
                integerStack.getHead().toString());

        integerStack.pop();

        assertEquals("[data = 2 next = [data = 1 next = null]]",
                integerStack.getHead().toString());
        assertEquals(2, integerStack.getSize());

        Stack<String> stringStack = new Stack<String>();
        stringStack.push("A");
        stringStack.push("B");
        stringStack.push("C");
        assertEquals("[data = C next = [data = B next = [data = A next = null]]]",
                stringStack.getHead().toString());

        stringStack.pop();

        assertEquals("[data = B next = [data = A next = null]]",
                stringStack.getHead().toString());
        assertEquals(2, stringStack.getSize());
    }
}
