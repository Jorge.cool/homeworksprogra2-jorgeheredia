package Stack;

import Stack.Exceptions.CalculatorException;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {
    @Test
    public void multiOperators() throws CalculatorException {
        Calculator calculator = new Calculator();
        assertEquals(8, calculator.calculate("3!+2"));
        calculator.reset();
        assertEquals(10, calculator.calculate("2^3+2"));
        calculator.reset();
        assertEquals(15, calculator.calculate("1+2*5"));
        calculator.reset();
        assertEquals(36, calculator.calculate("3!^2"));
        calculator.reset();
        assertEquals(48, calculator.calculate("4^2*3"));
    }

    @Test
    public void addition() throws CalculatorException {
        Calculator calculator = new Calculator();
        assertEquals(6, calculator.calculate("1+2+3"));
    }

    @Test
    public void multiplication() throws CalculatorException {
        Calculator calculator = new Calculator();
        assertEquals(20, calculator.calculate("2*2*5"));
    }

    @Test
    public void power() throws CalculatorException {
        Calculator calculator = new Calculator();
        assertEquals(8, calculator.calculate("2^3"));
    }

    @Test
    public void factorial() throws CalculatorException {
        Calculator calculator = new Calculator();
        assertEquals(24, calculator.calculate("4!"));
    }

    @Test
    public void badParenthesis() {
        Calculator calculator = new Calculator();
        try {
            calculator.calculate("1+(2+3))");
        } catch (CalculatorException e) {
            assertEquals("Stack.Exceptions.ParenthesisSyntaxException: The parentheses were not placed correctly",
                    e.toString());
        }
    }

    @Test
    public void OperatorNotAllowed() {
        Calculator calculator = new Calculator();
        try {
            calculator.calculate("1+2-3");
        } catch (CalculatorException e) {
            assertEquals("Stack.Exceptions.OperatorNotPermitted: The operator: '-'	IS NOT PERMITTED",
                    e.toString());
        }
    }

    @Test
    public void DoubleOperatorNotValid() {
        Calculator calculator = new Calculator();
        try {
            calculator.calculate("1+2+*3");
        } catch (CalculatorException e) {
            assertEquals("Stack.Exceptions.RepeatedOperator: The operators '+' and '*' they can't be together",
                    e.toString());
        }
    }
}
